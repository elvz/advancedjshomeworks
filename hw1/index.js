class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }
    get age() {
        return this._age
    }
    get salary() {
        return this._salary
    }

    set name(value) {
        this._name = value
    }
    set age(value) {
        this._age = value
    }
    set salary(value) {
        this._salary = +value
    }
}

class Programmer extends Employee {
    constructor(lang, name, age, salary) {
        super(name, age)
        this._lang = lang
        this._salary = salary * 3
    }

    get salary() {
        return +this._salary
    }
}

const developer = new Programmer('JS', 'Alex', 35, 3000)
const employee = new Employee('Oleg', 40, 5000)

console.log(developer);
console.log(employee);