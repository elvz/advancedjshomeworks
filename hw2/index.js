const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");

class Props extends Error {
  constructor(props) {
    super();
    this.name = "Error";
    this.message = `${props} is missed`;
  }
}

class SingleBook {
  constructor(book) {
    if (!book.author) {
      throw new Props("author");
    } else if (!book.name) {
      throw new Props("name");
    } else if (!book.price) {
      throw new Props("price");
    }
    this.author = book.author;
    this.name = book.name;
    this.price = book.price;
  }

  render(e) {
    e.insertAdjacentHTML(
      'beforeend',
      `<ul>
          <li>Author: ${this.author}</li>
          <li>Name: ${this.name}</li>
          <li>Price: ${this.price}</li>
        </ul>`
    );
  }
}

books.forEach((e) => {
  try {
    new SingleBook(e).render(root);
  } catch (err) {
    if (err instanceof Props) {
      console.error(err);
    } else {
      throw err;
    }
  } 
});
