const user1 = {
  name: "John",
  years: 30,
};
const div = document.getElementById("app");
const isAdminCheck = (obj) => {
  isAdmin = obj.hasOwnProperty("isAdmin");
  const user1 = {
    name: obj.name,
    age: obj.years,
    isAdmin: isAdmin,
  };
  return user1;
};

let template = `<ul>
  <li>name: ${isAdminCheck(user1).name}</li>
  <li>age: ${isAdminCheck(user1).age}</li>
  <li>isAdmin: ${isAdminCheck(user1).isAdmin}</li>
  </ul>
  `;

div.innerHTML = template;
