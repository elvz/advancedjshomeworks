const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

const extendsEmployee = {
    ...employee,
    age: 35,
    salary: '$3500'
} 

console.log(extendsEmployee);