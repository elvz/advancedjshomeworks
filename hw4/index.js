(function () {
  moviesProvider("https://ajax.test-danit.com/api/swapi/films").then(
    (movies) => {
      console.log(movies);
      render(movies);

      function render(list) {
        const views = new Array(list.length);

        for (let i = 0; i < list.length; i++) {
          const item = list[i];

          for (let j = 0; j < item.characters.length; j++) {
            
            fetch(item.characters[j])
              .then((res) => res.json())
              .then(characters => {
                let chars = []
                chars.forEach(ch => ch.push(characters))
                console.log(chars.length);
              });
            views[i] = `<div class="card">
                    <div>${item.elem}</div>
                    <div>${item.episodeId}</div>
                    <div>${item.name}</div>
                    <div>${item.openingCrawl}</div>
                    </div>
                    `;
          }
        }
        const app = document.getElementById("app");
        app.innerHTML = views.join("");
      }
    }
  );
})();
